package zikaVectorControl.zones;

import java.awt.Color;

public class Vegetation extends AttractionZone {
	public Vegetation(double x, double y, double radius) {
		super("Vegetation Zone",x, y, radius);
		this.color = Color.GREEN;
	}
	
}
