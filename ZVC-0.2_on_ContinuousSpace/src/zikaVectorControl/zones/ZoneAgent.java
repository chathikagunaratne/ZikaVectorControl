package zikaVectorControl.zones;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.query.space.gis.IntersectsQuery;
import repast.simphony.util.ContextUtils;

/**
 * The Zone agent defines a specific region in the geography that is 
 * represented by a polygon feature.  Zone agents will update the water status
 * of any GisAgent located within a certain proximity to the Zone.
 * 
 * @author Eric Tatara
 *
 */
public class ZoneAgent {
	private String name;
	private double taxRate;
	private double waterFlowRate;
	public ZoneAgent(String name){
		this.name = name;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getWaterFlowRate() {
		return waterFlowRate;
	}

	public void setWaterFlowRate(double waterFlowRate) {
		this.waterFlowRate = waterFlowRate;
	}
}