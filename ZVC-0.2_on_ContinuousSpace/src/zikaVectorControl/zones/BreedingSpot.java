package zikaVectorControl.zones;

import java.awt.Color;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.vector.Mosquito;

public class BreedingSpot extends AttractionZone {

	private int fetalCount; 
	
	public BreedingSpot(double x, double y, double radius){
		super("Breeding Spot",x, y, radius);
		this.color = Color.BLUE;
		fetalCount= 0;
	}

	public void addFetus(Mosquito egg) {
		egg.setBirthPlace(this);
		Context context = ContextUtils.getContext(this);
		context.add(egg);
		fetalCount++;
	}	
	public void hatch() {
		fetalCount--;
	}
	public void killOne() {
		fetalCount--;
	}
	public int getFetalCount() {
		return fetalCount;
	}	
	public void setFetalCount(int fetalCount) {
		this.fetalCount = fetalCount;
	}
	//monitoring
	@ScheduledMethod(start = 1, interval = 24, priority = ScheduleParameters.FIRST_PRIORITY)
	public void step() {
		Context context = ContextUtils.getContext(this);
		if (context.getObjects(Mosquito.class).size() == 0) {
			RunEnvironment.getInstance().endRun();
		}
	}
}
