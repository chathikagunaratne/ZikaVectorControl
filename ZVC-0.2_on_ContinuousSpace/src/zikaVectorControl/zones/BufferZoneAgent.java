package zikaVectorControl.zones;

import com.vividsolutions.jts.geom.Geometry;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.gis.Geography;
import repast.simphony.space.gis.GeometryUtils;
import repast.simphony.util.ContextUtils;


/**
 * BufferZoneAagent is a geolocated agent indicated by a polygon feature that
 *  represents a buffer around a ZoneAgetn.
 * 
 * @author Eric Tatara
 *
 */
public class BufferZoneAgent {

	private double size;  // size of the current buffer zone.
	private String name; 
  private ZoneAgent zone;  // Zone that's associated with this bufferzone
	
	public BufferZoneAgent(String name, double size, ZoneAgent zone){
		this.name = name;
		this.size = size;
		this.zone = zone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}