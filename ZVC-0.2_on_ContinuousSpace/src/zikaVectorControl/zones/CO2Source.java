package zikaVectorControl.zones;

import java.awt.Color;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.gis.util.GeometryUtil;
import repast.simphony.parameter.Parameters;
import repast.simphony.query.space.gis.IntersectsQuery;
import repast.simphony.space.gis.GeometryUtils;
import repast.simphony.space.grid.GridPoint;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.vector.Mosquito.BehaviorMode;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;

public class CO2Source extends AttractionZone{
	public CO2Source(double x, double y, double radius){
		super("CO2 Source", x, y, radius);
		this.color = Color.YELLOW;
	}
}
