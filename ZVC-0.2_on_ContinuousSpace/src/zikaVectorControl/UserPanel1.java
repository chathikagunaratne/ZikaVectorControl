//package zikaVectorControl;
//
//import java.awt.Event;
//import java.awt.LayoutManager;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//
//import javax.swing.BoxLayout;
//import javax.swing.JButton;
//import javax.swing.JPanel;
//import javax.swing.JTextField;
//
//import repast.simphony.context.Context;
//import repast.simphony.engine.environment.RunEnvironment;
//import repast.simphony.parameter.Parameters;
//import repast.simphony.space.continuous.ContinuousSpace;
//import repast.simphony.userpanel.ui.UserPanelCreator;
//import repast.simphony.util.ContextUtils;
//import zikaVectorControl.Mosquito.LifeStages;
//
//public class UserPanel1 implements UserPanelCreator{
//	
//	static Context context;
//
//	@Override
//	public JPanel createPanel() {
//		JPanel panel1 = new JPanel();
//		panel1.setLayout( new BoxLayout(panel1, BoxLayout.Y_AXIS));
//		JButton wolbachiaButton = new JButton("add Wolbachia");
//		JTextField wolbachiaCountField = new JTextField(30);
//		
//		//wolbachiaButton.action(new Event(wolbachiaButton, EventT, arg2), what)
//	    //Context context = ContextUtils.getContext(this); 
//	    // add the listener to the jbutton to handle the "pressed" event
//		wolbachiaButton.addActionListener(new ActionListener()
//	    {
//	     
//		@Override
//		public void actionPerformed(ActionEvent arg0) {
//			// TODO Auto-generated method stub
//			int wolbachiaCount = Integer.valueOf(wolbachiaCountField.getText());
//			//((PopulationMonitor) context.getObjects(PopulationMonitor.class).get(0)).infect(wolbachiaCount);
//		}
//	    });
//		panel1.add(wolbachiaCountField);
//		panel1.add(wolbachiaButton);
//		
//		
//		JButton gmoButton = new JButton("add GMOs");
//		JTextField gmoCountField = new JTextField(30);
//		
//		gmoButton.addActionListener(new ActionListener()
//	    {
//	     
//		@Override
//		public void actionPerformed(ActionEvent arg0) {
//			// TODO Auto-generated method stub
//			Parameters parm = RunEnvironment.getInstance().getParameters();
//			//int initialWolbachia = (Integer)parm.getValue("initialWolbachia");
//			int gmoCount = Integer.valueOf(gmoCountField.getText());
//			double temperature = ((ClimateController) context.getObjects(ClimateController.class).get(0)).getTemperature();
//			System.out.println(temperature);
//				
//			char sex = 'f';
//			for (int i = 0; i < gmoCount; i++) {
//				int index = (int)Math.random() * (context.getObjects(BreedingSpot.class).size() - 1);
//				BreedingSpot birthPlace = (BreedingSpot) context.getObjects(BreedingSpot.class).get(index);
//				if(i > 5)
//					sex = 'm';
//				AedesAegypti agent = new AedesAegypti(birthPlace.x,birthPlace.y,sex,null,temperature, context);
//				agent.setBirthPlace(birthPlace);			
//				//just age them into adults and add
//				agent.setAsRandomlyAgedAdult();
//				agent.setLifeStage(LifeStages.ADULT);
//				agent.emerged = true;
//				agent.setDominantLethalGene(true);
//				context.add(agent);
//				((ContinuousSpace)context.getProjection("Space")).moveTo(agent, birthPlace.x,birthPlace.y);
//			}
//		}
//	    });
//		
//		panel1.add(gmoCountField);
//		panel1.add(gmoButton);
//		return panel1;
//	}
//
//}
