/**
 * 
 */
package zikaVectorControl.vector;

import java.util.Random;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;

import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;
import repast.simphony.space.grid.GridPoint;
import zikaVectorControl.GisAgent;
import zikaVectorControl.Host;
import zikaVectorControl.Virus;

/**
 * @author cgunaratne
 * Defines the basic features of a Vector
 */
public abstract class Vector extends GisAgent{
	
	/**
	 * Vector is a repast agent
	 */
	
	enum ModeOfMovement {WALKING, FLYING}
	
	/**
	 * Vector has arbovirus and a host
	 */
	private Virus virus;
	private Host host;
	private boolean carrier;
	
	
	
	public Vector(double x, double y) {
		super("id",x,y);		
	}
	
	/*
	 * Getters setters
	 * 
	 */
	public Virus getVirus() {
		return virus;
	}
	public void setVirus(Virus virus) {
		this.virus = virus;
	}
	public Host getHost() {
		return host;
	}
	public void setHost(Host host) {
		this.host = host;
	}
	public boolean isCarrier() {
		return carrier;
	}
	public void setCarrier(boolean carrier) {
		this.carrier = carrier;
	}
	
	
}
