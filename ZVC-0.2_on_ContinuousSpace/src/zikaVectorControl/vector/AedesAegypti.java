/**
 * 
 */
package zikaVectorControl.vector;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.grid.Grid;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.GisAgent;
import zikaVectorControl.Host;
import zikaVectorControl.controllers.ClimateController;
import zikaVectorControl.vector.Mosquito.BehaviorMode;
import zikaVectorControl.vector.Mosquito.LifeStages;

/**
 * @author cgunaratne
 *
 */
public class AedesAegypti extends Mosquito {
	boolean wolbachiaInfected;
	boolean dominantLethalGene;
	int timesOvipositioned = 0;
	int eggsLaid = 0;
	int adult;
	Parameters parm;
	Context context;
	double temperature = 293.706;

	public AedesAegypti(double x, double y,char sex, Host human, double temperature, Context context){
		super(x,y,sex, human);
		parm = RunEnvironment.getInstance().getParameters();
		this.setContext(context);
		this.setMovementSpeedLow((Double)parm.getValue("lowSpeed"));
		this.setMovementSpeedHigh((Double)parm.getValue("highSpeed"));
		this.setCurrentSpeed(this.getMovementSpeedLow());
		
		this.setCurrentBehavior(BehaviorMode.STATIONARY);
		//this.setLifeCycleDurations(3, 15 , 3 , (Integer)parm.getValue("adultLifeExpectancy"));
		this.setHeading(Math.PI/4);
		if(this.sex == 'f') {
			setParametersFemale();
		} else {
			setParametersMale();
		}
		updateFetalMortalityRateByTemperature(temperature);
		updateAdultMortalityRateByTemperature(temperature);
		this.setpEmergence((Double)parm.getValue("pEmergenceMortality"));
	}
	
	@ScheduledMethod(start = 1, interval = 1, priority =ScheduleParameters.FIRST_PRIORITY)
	public void step() {
		this.fly();
	}
	
	private void setParametersFemale() {
		this.setpDieByHuman(0.02);
		this.setpMaleChild(0.5);
		this.setEggsPerBatch(60);
		this.setDurationOfOvipositioning(3);
	}
	private void setParametersMale() {
		this.setEggsPerBatch(0);
		this.setMatesPerMosquito(5);
		this.setpMate((Double)parm.getValue("pMaleMating"));
	}
	
	public boolean oviposition() {
		boolean done = false;
		if(lifeStage == LifeStages.ADULT) {
			//female fertilized
			if(sex == 'f' && timesOvipositioned < 5) {
				fertilized = false;
				setFed(false);
				
				double eggsInThisStep = 0; 		
				Context context = ContextUtils.getContext(this);
				if(context == null || context.getObjects(ClimateController.class).size() < 1 ) {
					eggsInThisStep = (double) ((this.getEggsPerBatch() / durationOfOvipositioning)/matingTimeSectors().length);					
				} else{
					//System.out.println("eggs " + ((ClimateController)context.getObjects(ClimateController.class).get(0)).getOvipositionRateAtThisTemperature() );
					eggsInThisStep = (double)(((ClimateController)context.getObjects(ClimateController.class).get(0)).getOvipositionRateAtThisTemperature() / matingTimeSectors().length);
					//eggsInThisStep = (double)(63 / matingTimeSectors().length);
				}
				
				
				for (int i =0; i< eggsInThisStep;  i++) {
					double p = Math.random();
					char childsSex = 'f';
					if(p < getpMaleChild()) {
						childsSex = 'm';
					}					
					Context parentContext = ContextUtils.getContext(this);
					AedesAegypti child = new AedesAegypti(getBreedingSpot().getX(), getBreedingSpot().getY(), childsSex, null, temperature, parentContext);
					child.setAge(0);
					child.setLifeStage(LifeStages.EGG);
					p = Math.random();
					child.setWolbachiaInfected(this.wolbachiaInfected, mateHadWolbachia);
					child.setDominantLethalGene(this.dominantLethalGene, mateWasRIDL);
					
					closestBreedingSpot.addFetus(child);
					
					eggsLaid++;
				}
				
			}
			if(eggsLaid > eggsPerBatch) {
				done = true;
				timesOvipositioned ++;
				eggsLaid = 0;				
				this.mateWasRIDL = false;
			}
		}
		return done;
	}
	
	public void setAsRandomlyAgedAdult() {
		int age = (int) Math.round(fetalDuration + Math.random() * (adultDuration)  +2);
		this.setAge(age);
	}
	public void updateFetalMortalityRateByTemperature(double temperature) {
		// temperature in kelvins
		if(this.getContext()!= null && this.getContext().getObjects(ClimateController.class).size() > 0) {
			this.temperature = ((ClimateController)(this.getContext().getObjects(ClimateController.class).get(0))).getTemperature();
			double fetalMortalityRate = ((ClimateController)(context.getObjects(ClimateController.class).get(0))).getFetalMortalityRateAtThisTemperature();
			double fetalDuration = ((ClimateController)(context.getObjects(ClimateController.class).get(0))).getFetalDurationThisTemperature();
			this.setFetalMortality(fetalMortalityRate);
			this.setFetalDuration(fetalDuration);
		}else {
			this.temperature = temperature;
			double tempInC = this.temperature - 273.15;
			double fetalMortalityRate = 0.5412630  -0.0445754*tempInC + 0.0009371*Math.pow(tempInC,2);
			double fetalDuration = 217.79077 -14.17510*tempInC + 0.23573*Math.pow(tempInC,2);
			this.setFetalMortality(fetalMortalityRate);
			this.setFetalDuration(fetalDuration);
		}
	}
	public void updateAdultMortalityRateByTemperature(double temperature) {
		if(this.dominantLethalGene || this.wolbachiaInfected)
			return;
		// temperature in kelvins
		if(this.getContext()!= null && this.getContext().getObjects(ClimateController.class).size() > 0) {
			this.temperature = ((ClimateController)(this.getContext().getObjects(ClimateController.class).get(0))).getTemperature();
			double adultMortality = ((ClimateController)(context.getObjects(ClimateController.class).get(0))).getAdultMortalityRatesAtThisTemperature();
			double adultDuration = ((ClimateController)(context.getObjects(ClimateController.class).get(0))).getAdultDurationAtCurrentTemperature();
//			if(sex == 'f') {
//				adultMortality *= (Double)parm.getValue("mortalityPerDayAdultFemale");
//			} else {
//				adultMortality *= (Double)parm.getValue("mortalityPerDayAdultMale");
//			}
			this.adultMortality = adultMortality;
			this.setAdultDuration(adultDuration);
		}else {
			this.temperature = temperature;
			double tempInC = this.temperature - 273.15;
			//double adultMortality = 0.3718  + 0.04995*tempInC  - 0.0007068*Math.pow(tempInC,2) - 0.00001080 * Math.pow(tempInC,3);
			double adultMortality = 0.1897 -0.010330*tempInC + 0.0002718*Math.pow(tempInC,2);
			double adultDuration = -30.82832 +5.28638*tempInC -0.11095*Math.pow(tempInC,2);
			if(sex == 'f') {
				//adultMortality *= (Double)parm.getValue("mortalityPerDayAdultFemale");
			} else {
				//adultMortality *= (Double)parm.getValue("mortalityPerDayAdultMale");
			}
			this.adultMortality = adultMortality;
			setAdultDuration(adultDuration);
		}
	}
	@Override
	public void emerge() {
		if(isEmerged()) {
			//System.out.println("emergence error");
			return;
		}
		//kill emerging pupa
		double p = Math.random();
		if (p < this.getpEmergence()){
			die();
		} else {
			Context context = ContextUtils.getContext(this);
			ContinuousSpace<GisAgent> space = (ContinuousSpace) context.getProjection("Space");
			this.birthPlace.hatch();
			space.moveTo(this, this.birthPlace.getX(),this.birthPlace.getY());
			updateAdultMortalityRateByTemperature(((ClimateController)context.getObjects(ClimateController.class).get(0)).getTemperature());
			this.setFed(false);
			this.setEmerged(true);
		}
	}
	public boolean isWolbachiaInfected(){
		return wolbachiaInfected;
	}
	public int getWolbachiaInfected() {
		if(wolbachiaInfected)
			return 1;
		return 0;
	}

	public void setWolbachiaInfected(boolean motherWolbachiaInfected, boolean fatherWolbachiaInfected) {
		if(!motherWolbachiaInfected && fatherWolbachiaInfected) {
			this.wolbachiaInfected = true;
			this.setFetalDuration(0);
			this.setAdultDuration(0);
		}
		if(motherWolbachiaInfected) {
			this.wolbachiaInfected = true;
			//life span is shortened due to wolbachia infection
			this.setAdultDuration( this.getAdultDuration() * (Double)parm.getValue("adultLifeExpectancyReductionByWolbachia")); 
			//System.out.println(this.getAdultDuration() + " " +(Double)parm.getValue("adultLifeExpectancyReductionByWolbachia"));
		}
		if(!motherWolbachiaInfected && !fatherWolbachiaInfected) 
			this.wolbachiaInfected = false;
	}
	
	public int getDominantLethalGene() {
		if(dominantLethalGene)
			return 1;
		return 0;
	}
	public boolean isDominantLethalGene(){
		return dominantLethalGene;
	}
	public void setDominantLethalGene(boolean motherHadDominantLethalGene, boolean fatherHadDominantLethalGene) {
		if(motherHadDominantLethalGene || fatherHadDominantLethalGene) {
			this.dominantLethalGene = true;
			this.setAdultDuration(0);
			this.setpMate(this.getpMate()/2);
		} else {
			this.dominantLethalGene = false;
		}
	}
	public void setDominantLethalGeneCarrier(){
		this.dominantLethalGene = true;
		this.setpMate(this.getpMate()/2);
	}
	public int getAdult() {
		if(lifeStage == LifeStages.ADULT)
			adult = 1;
		return adult;
	}

	public void setAdult(int adult) {
		this.adult = adult;
	}
	
	public int isBitingAge() {
		if(getAge() > fetalDuration + 2) {
			return 1;
		}
		return 0;
	}
	public int isAdultFemale() {
		if(getLifeStage() == LifeStages.ADULT && sex == 'f') {
			return 1;
		}
		return 0;
	}
	public int isAdultMale() {
		if(getLifeStage() == LifeStages.ADULT && sex == 'm') {
			return 1;
		}
		return 0;
	}
	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}
	
}
