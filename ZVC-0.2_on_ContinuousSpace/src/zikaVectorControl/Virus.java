/**
 * 
 */
package zikaVectorControl;

import java.util.LinkedList;

/**
 * @author cgunaratne
 *
 */
public abstract class Virus {

	private LinkedList<Host> potentialHosts;
	private String virusName;
	private String alias;
	
	public String getVirusName() {
		return virusName;
	}
	public void setVirusName(String virusName) {
		this.virusName = virusName;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	
	
}
