package zikaVectorControl.styles;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Offset;
import gov.nasa.worldwind.render.PatternFactory;
import gov.nasa.worldwind.render.SurfacePolygon;
import gov.nasa.worldwind.render.SurfaceShape;
import gov.nasa.worldwind.render.WWTexture;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.image.BufferedImage;

import repast.simphony.visualization.gis3D.BufferedImageTexture;
import repast.simphony.visualization.gis3D.PlaceMark;
import repast.simphony.visualization.gis3D.style.MarkStyle;
import repast.simphony.visualization.gis3D.style.SurfaceShapeStyle;
import zikaVectorControl.zones.CO2Source;
import zikaVectorControl.zones.ZoneAgent;

/**
 * Style for ZoneAgents.
 * 
 * @author Eric Tatara
 *
 */
public class ZoneStyle implements SurfaceShapeStyle<ZoneAgent>{

	@Override
	public SurfaceShape getSurfaceShape(ZoneAgent object, SurfaceShape shape) {
		return new SurfacePolygon();
	}

	@Override
	public Color getFillColor(ZoneAgent zone) {
		if(zone instanceof CO2Source){
			return Color.YELLOW;
		}else{
			return Color.CYAN;
		}
	}

	@Override
	public double getFillOpacity(ZoneAgent obj) {
		return 0.25;
	}

	/**
	 * If the zone has water then indicate with a BLUE outline.
	 */
	@Override
	public Color getLineColor(ZoneAgent zone) {
		if (zone.getWaterFlowRate() > 0)
			return Color.blue;
		else
			return Color.black;
	}

	@Override
	public double getLineOpacity(ZoneAgent obj) {
		return 1.0;
	}

	@Override
	public double getLineWidth(ZoneAgent obj) {
		return 3;
	}
	
	
}