package zikaVectorControl.styles;

import gov.nasa.worldwind.render.PatternFactory;
import gov.nasa.worldwind.render.WWTexture;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;

import repast.simphony.visualization.gis3D.BufferedImageTexture;
import zikaVectorControl.GisAgent;
import zikaVectorControl.vector.Mosquito;

public class MosquitoStyle extends GisAgentStyle{
	public WWTexture getTexture(GisAgent agent, WWTexture texture) {
		
		// WWTexture is null on first call.
		
		Color color = null;
	
		
		color = Color.YELLOW;
		if(agent instanceof Mosquito){
			Mosquito m = (Mosquito) agent;
			if(m.isFed()) {
				color = Color.GREEN;
			}
			if(m.getSex() == 'f' && m.isFertilized()) {
				color = Color.RED;
			}
		}
		BufferedImage image = PatternFactory.createPattern(PatternFactory.PATTERN_TRIANGLE_UP, 
				new Dimension(10, 10), 0.7f,  color);

		return new BufferedImageTexture(image);	
	}
}
