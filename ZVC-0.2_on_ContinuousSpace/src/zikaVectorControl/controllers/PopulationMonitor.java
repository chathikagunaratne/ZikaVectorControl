package zikaVectorControl.controllers;

import repast.simphony.context.Context;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.vector.AedesAegypti;
import zikaVectorControl.vector.Mosquito.LifeStages;
import zikaVectorControl.zones.BreedingSpot;
import zikaVectorControl.zones.CO2Source;

public class PopulationMonitor {
	
	int numberDeadThisStep;
	int totalNumberDead;
	int meanDeathAgeForStep;
	int releases = 1;
	
	public PopulationMonitor() {
		numberDeadThisStep = 0;
		totalNumberDead = 0;
	}
	
	public void incrementDeaths(int age){
		numberDeadThisStep++;
		meanDeathAgeForStep = (age + meanDeathAgeForStep) / numberDeadThisStep;
	}
	
	@ScheduledMethod( start = 1.1 , interval = 1, priority = ScheduleParameters.LAST_PRIORITY)
	public void updateCounters(){
		totalNumberDead += numberDeadThisStep;
		numberDeadThisStep = 0;
		meanDeathAgeForStep = 0;
		//@ScheduledMethod(start = 21900, interval = 1000000000, priority = ScheduleParameters.FIRST_PRIORITY)
		
	}
	//5040 is july  using april as custom
	@ScheduledMethod(start = 20160, interval = 180, priority = ScheduleParameters.FIRST_PRIORITY)
	public void infect1() {
		Parameters parm = RunEnvironment.getInstance().getParameters();
		//int initialWolbachia = (Integer)parm.getValue("initialWolbachia");
		Context context = ContextUtils.getContext(this);
		double temperature = ((ClimateController) context.getObjects(ClimateController.class).get(0)).getTemperature();
		int numUrbanPoints = context.getObjects(CO2Source.class).size();
		System.out.println(numUrbanPoints);
		//numbers from Nguyen et al. 2015 Machan's Beach study
		// release 11 males and 11 females a week per urban point
		int maleCount = 11; 
		int femaleCount = 11;
		float freqReleasePoints = 0.25f;
		System.out.println(freqReleasePoints);
		//int index = (int)Math.random() * (context.getObjects(BreedingSpot.class).size() - 1);
		if(releases > 15)
			return;
		for (int i = 0; i < numUrbanPoints * freqReleasePoints; i++) {
			int index = Math.round((int)Math.random() * (context.getObjects(CO2Source.class).size() - 1));
			CO2Source ReleasePoint = (CO2Source) context.getObjects(CO2Source.class).get(index); 
			for (int j = 0; j < maleCount; j++) {
				char sex = 'm';
				AedesAegypti agent = new AedesAegypti(ReleasePoint.getX(),ReleasePoint.getY(),sex,null,temperature, context);
				agent.setBirthPlace(null);			
				//just age them into adults and add
				agent.setAge((int) (agent.getFetalDuration()+1));
				agent.setLifeStage(LifeStages.ADULT);
				agent.setEmerged(true);
				agent.setWolbachiaInfected(true,false);
				context.add(agent);
				((ContinuousSpace)context.getProjection("Space")).moveTo(agent, ReleasePoint.getX(),ReleasePoint.getY());
			}
			for (int j = 0; j < femaleCount; j++) {
				char sex = 'f';
				AedesAegypti agent = new AedesAegypti(ReleasePoint.getX(),ReleasePoint.getY(),sex,null,temperature, context);
				agent.setBirthPlace(null);			
				//just age them into adults and add
				agent.setAge((int) (agent.getFetalDuration()+1));
				agent.setLifeStage(LifeStages.ADULT);
				agent.setEmerged(true);
				agent.setWolbachiaInfected(true,false);
				context.add(agent);
				((ContinuousSpace)context.getProjection("Space")).moveTo(agent, ReleasePoint.getX(),ReleasePoint.getY());}
			}
		releases++;
	}
	@ScheduledMethod(start = 21960, interval = 180, priority = ScheduleParameters.FIRST_PRIORITY)
	public void infect2() {
		Parameters parm = RunEnvironment.getInstance().getParameters();
		//int initialWolbachia = (Integer)parm.getValue("initialWolbachia");
		Context context = ContextUtils.getContext(this);
		double temperature = ((ClimateController) context.getObjects(ClimateController.class).get(0)).getTemperature();
		int numUrbanPoints = context.getObjects(CO2Source.class).size();
		System.out.println(numUrbanPoints);
		//numbers from Nguyen et al. 2015 Machan's Beach study
		// release 11 males and 11 females a week per urban point
		int maleCount = 6; 
		int femaleCount = 0;
		float freqReleasePoints = 0.25f;
		System.out.println(freqReleasePoints);
		//int index = (int)Math.random() * (context.getObjects(BreedingSpot.class).size() - 1);
		if(releases > 10)
			return;
		for (int i = 0; i < numUrbanPoints * freqReleasePoints; i++) {
			int index = Math.round((int)Math.random() * (context.getObjects(CO2Source.class).size() - 1));
			CO2Source ReleasePoint = (CO2Source) context.getObjects(CO2Source.class).get(index); 
			for (int j = 0; j < maleCount; j++) {
				char sex = 'm';
				AedesAegypti agent = new AedesAegypti(ReleasePoint.getX(),ReleasePoint.getY(),sex,null,temperature, context);
				agent.setBirthPlace(null);			
				//just age them into adults and add
				agent.setAge((int) (agent.getFetalDuration()+1));
				agent.setLifeStage(LifeStages.ADULT);
				agent.setEmerged(true);
				agent.setWolbachiaInfected(true,false);
				context.add(agent);
				((ContinuousSpace)context.getProjection("Space")).moveTo(agent, ReleasePoint.getX(),ReleasePoint.getY());
			}
			for (int j = 0; j < femaleCount; j++) {
				char sex = 'f';
				AedesAegypti agent = new AedesAegypti(ReleasePoint.getX(),ReleasePoint.getY(),sex,null,temperature, context);
				agent.setBirthPlace(null);			
				//just age them into adults and add
				agent.setAge((int) (agent.getFetalDuration()+1));
				agent.setLifeStage(LifeStages.ADULT);
				agent.setEmerged(true);
				agent.setWolbachiaInfected(true,false);
				context.add(agent);
				((ContinuousSpace)context.getProjection("Space")).moveTo(agent, ReleasePoint.getX(),ReleasePoint.getY());}
			}
		releases++;
	}
//	@ScheduledMethod(start = 22320, interval = 720, priority = ScheduleParameters.FIRST_PRIORITY)
//	public void RIDL() {
//		Parameters parm = RunEnvironment.getInstance().getParameters();
//		//int initialGMO = (Integer)parm.getValue("initialGMO");
//		Context context = ContextUtils.getContext(this);
//		double temperature = ((ClimateController) context.getObjects(ClimateController.class).get(0)).getTemperature();
//		int count = 1720;
//		char sex = 'm';
//		
//		int index = (int)Math.random() * (context.getObjects(BreedingSpot.class).size() - 1);
//		if(releases > 4)
//			return;
//		for (int i = 0; i < count; i++) {
//			BreedingSpot birthPlace = (BreedingSpot) context.getObjects(BreedingSpot.class).get(index);
//			AedesAegypti agent = new AedesAegypti(birthPlace.x,birthPlace.y,sex,null,temperature, context);
//			agent.setBirthPlace(birthPlace);			
//			//just age them into adults and add
//			agent.setAsRandomlyAgedAdult();
//			agent.setLifeStage(LifeStages.ADULT);
//			agent.emerged = true;
//			agent.setDominantLethalGeneCarrier();
//			context.add(agent);
//			((ContinuousSpace)context.getProjection("Space")).moveTo(agent, birthPlace.x,birthPlace.y);
//		}
//		releases++;
//	}
	public int getNumberDeadThisStep(){
		return numberDeadThisStep;
	}	
	
	public int getMeanDeathAgeForStep(){
		return meanDeathAgeForStep;
	}
}
