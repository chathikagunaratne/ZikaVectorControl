package zikaVectorControl.controllers;

import repast.simphony.context.Context;
import repast.simphony.engine.schedule.ScheduleParameters;
import repast.simphony.engine.schedule.ScheduledMethod;
import repast.simphony.util.ContextUtils;
import zikaVectorControl.vector.AedesAegypti;
import zikaVectorControl.vector.Mosquito;

public class ClimateController {
	
	private int month;
	double temperature;
	private double fetalDurationAtThisTemperature;
	private double adultDurationAtThisTemperature;
	private double fetalMortalityAtThisTemperature;
	private double adultMortalityRateAtThisTemperature;
	private double ovipositionRate;
	
	public ClimateController() {
		this.month = 1;
		
	}
	/**
	 * This function will run every month to:
	 * - Update the temperature of the mosquitoes
	 * - Update the mortality rates according to (Otero, 2006) 
	 * 
	 * Additionally, this function starts after 10000 ticks (reasonable time to initialize normal behavior in sim
	 */
	@ScheduledMethod(start = 1, interval = 720, priority = ScheduleParameters.FIRST_PRIORITY)
	public void controlClimate (){
		
		temperature = 293.706;
		double monthlyTemperature[] = {293.706, 293.45556, 298.06667, 300.01111, 300.56667, 
				301.9, 303.06667, 303.0389, 302.5389, 300.9, 299.56667, 298.4833};
		
		temperature = monthlyTemperature[month-1];
		
		month++;
		if(month > 12) 
			month = 1;
		double tempInC = this.temperature - 273.15;
		
//		double eggMortality = 0.01;
//		double larvalMortality = 0.01 + 0.9725*Math.exp(-(temperature - 278)/2.7035);
//		double pupalMortality = 0.01 + 0.9725*Math.exp(-(temperature - 278)/2.7035);
		
		//adult mortality, adult duration, and oviposition rate change with temperature
		fetalMortalityAtThisTemperature = 0.5412630  -0.0445754*tempInC + 0.0009371*Math.pow(tempInC,2);
		adultMortalityRateAtThisTemperature = 0.1897 -0.010330*tempInC + 0.0002718*Math.pow(tempInC,2);
		fetalDurationAtThisTemperature = 217.79077 -14.17510*tempInC + 0.23573*Math.pow(tempInC,2);
		adultDurationAtThisTemperature = -30.82832 +5.28638*tempInC -0.11095*Math.pow(tempInC,2);
		ovipositionRate = 22.763452 -3.861519*tempInC + 0.203669*Math.pow(tempInC,2) -0.003017*Math.pow(tempInC,3); 
		
		Context context = ContextUtils.getContext(this);
		for (Object m : context.getObjects(Mosquito.class)) {
			((AedesAegypti) m).updateFetalMortalityRateByTemperature(temperature);
			((AedesAegypti) m).updateAdultMortalityRateByTemperature(temperature);
		}
		
	}
	
	public double getAdultDurationAtCurrentTemperature() {
		return adultDurationAtThisTemperature;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public double getTemperature() {
		return temperature;
	}
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	public double getFetalMortalityRateAtThisTemperature() {
		return fetalMortalityAtThisTemperature;
	}
	public double getAdultMortalityRatesAtThisTemperature() {
		return adultMortalityRateAtThisTemperature;
	}
	public double getOvipositionRateAtThisTemperature() {
		return ovipositionRate;
	}
	public double getFetalDurationThisTemperature() {
		return fetalDurationAtThisTemperature;
	}
}
