package zikaVectorControl;

import java.util.HashMap;

public abstract class Host {
	
	HashMap<Virus,VirusHostProperties> susceptibleViruses;
	
	public Host(){
		susceptibleViruses = new HashMap<Virus,VirusHostProperties>();
	}
	public Virus getInucbationPeriod(String virusName) {
		Virus result = null;
		for(Virus susceptibleVirus: susceptibleViruses.keySet()) {
			if(susceptibleVirus.getVirusName().equalsIgnoreCase(virusName))
				result = susceptibleVirus;
		}
		return result;
	}

	
	public void setIncubationPeriod(Virus virus) {
		// TODO Auto-generated method stub
		
	}

	
	public String getVirusName() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void setVirusName(String name) {
		// TODO Auto-generated method stub
		
	}

	
	public void setProbabilityOfInfection() {
		// TODO Auto-generated method stub
		
	}

	
	public double getProbabilityOfInfection() {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public void setSymptomaticPeriod() {
		// TODO Auto-generated method stub
		
	}

	
	public double getSymptomaticPeriod() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	class VirusHostProperties {
		
	}
}
