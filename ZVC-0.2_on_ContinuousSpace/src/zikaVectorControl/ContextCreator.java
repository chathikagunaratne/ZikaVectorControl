package zikaVectorControl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.opengis.feature.simple.SimpleFeature;

import repast.simphony.context.Context;
import repast.simphony.context.space.continuous.ContinuousSpaceFactoryFinder;
import repast.simphony.context.space.gis.GeographyFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.RandomCartesianAdder;
import repast.simphony.space.gis.Geography;
import repast.simphony.space.gis.GeographyParameters;
import zikaVectorControl.zones.BreedingSpot;
import zikaVectorControl.zones.CO2Source;
import zikaVectorControl.zones.Vegetation;
import zikaVectorControl.controllers.ClimateController;
import zikaVectorControl.controllers.PopulationMonitor;
import zikaVectorControl.vector.AedesAegypti;
import zikaVectorControl.vector.Mosquito.LifeStages;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

/**
 * ContextBuilder for the GIS demo.  In this model, mobile GisAgents move around
 * the Geography with a random motion and are represented by point locations.  
 * ZoneAgents are polygons that represent certain geographic areas.  WaterLine
 * agents represent water supply lines from Lake Michigan that supply the 
 * Chicago area.  When a ZoneAgent intersects a WaterLine, the ZoneAgent will 
 * have access to fresh drinking water.  GisAgents that are within a certain 
 * distance from the ZoneAgent boundary will also have access to water.  Agents
 * that are not in proximity to a Zone with a water supply will not have access
 * to water (they will be thirsty).  BufferZoneAgents are for visualization 
 * to illustrate the extend of the boundary around a ZoneAgent.  
 * 
 * GisAgents may be generated programmatically depending on the value for 
 * number of agents.  GisAgents, ZoneAgents, and WaterLine agents are also
 * loaded from ESRI shapefiles.
 * 
 * @author Eric Tatara
 *
 */
public class ContextCreator implements ContextBuilder {

	int numAgents;
	double zoneDistance;
	double co2Radius;
	int TimeOfDay = 0; //1-24 ... > 12 means night
	double pDomesticBreedingSpot;
	Context<Object> c;
	static int ticksInADay;
	double numberOfBreedingSpots = 0;
	double larvaPerBreedingSpot;
	int initialWolbachia;
	int initialGMO;
	double dimensions [];
	double initialTemperature = 293.706;
	
	public Context<Object> build(Context context) {
		
		System.out.println("Geography Demo ContextBuilder.build()");
		c = context;
		Parameters parm = RunEnvironment.getInstance().getParameters();
		co2Radius = (Double)parm.getValue("co2Radius");  // meters
		pDomesticBreedingSpot = (Double)parm.getValue("pDomesticBreedingSpot");
		larvaPerBreedingSpot = (Integer)parm.getValue("larvaPerBreedingSpot");
		initialWolbachia = (Integer)parm.getValue("initialWolbachia");
		initialGMO = (Integer)parm.getValue("initialGMO");
		ticksInADay = parm.getInteger("ticksPerDay");
		
		dimensions = new double[]{200,200};
		
		ContinuousSpace<Object> space = ContinuousSpaceFactoryFinder.createContinuousSpaceFactory(null)
				.createContinuousSpace("Space", context, new RandomCartesianAdder<Object> (), new repast.simphony.space.continuous.WrapAroundBorders(), dimensions[0], dimensions[1]);
		GeographyParameters geoParams = new GeographyParameters();
		Geography geography = GeographyFactoryFinder.createGeographyFactory(null)
				.createGeography("Geography", context, geoParams);
		
		loadFeatures( "data/Zones3.shp", context, space);
		ClimateController cc = new ClimateController();
		context.add(cc);
		PopulationMonitor pm = new PopulationMonitor();
		context.add(pm);
		
		RunEnvironment.getInstance().endAt(17280 + 25920); // 2 + 3 years
		//RunEnvironment.getInstance().pauseAt(10000);
		return context;
	}
	public void addMosquitoes(Context<Object> context, ContinuousSpace<Object> space, BreedingSpot birthPlace){
		char sex = 'f';
		
		for (int i = 0; i < larvaPerBreedingSpot; i++) {
			if(i > larvaPerBreedingSpot/2)
				sex = 'm';
			AedesAegypti agent = new AedesAegypti(birthPlace.getX(),birthPlace.getY(),sex,null,initialTemperature, c);
			agent.setBirthPlace(birthPlace);			
			//just age them into adults and add
			agent.setAsRandomlyAgedAdult();
			agent.setLifeStage(LifeStages.ADULT);
			agent.setEmerged(true);
			GeometryFactory fac = new GeometryFactory();
			
			context.add(agent);
			space.moveTo(agent, birthPlace.getX(),birthPlace.getY());
		}
//		for (int i = 0; i < initialWolbachia; i++) {
//			if(i > initialWolbachia/2)
//				sex = 'm';
//			AedesAegypti agent = new AedesAegypti(birthPlace.x,birthPlace.y,sex,null,initialTemperature,c);
//			agent.setBirthPlace(birthPlace);			
//			//just age them into adults and add
//			agent.setAsRandomlyAgedAdult();
//			agent.setLifeStage(LifeStages.ADULT);
//			GeometryFactory fac = new GeometryFactory();
//			agent.wolbachiaInfected = true;
//			agent.emerged = true;
//			context.add(agent);
//			space.moveTo(agent, birthPlace.x,birthPlace.y);
//		}
//		for (int i = 0; i < initialGMO; i++) {
//			sex = 'm';
//			AedesAegypti agent = new AedesAegypti(birthPlace.x,birthPlace.y,sex,null,initialTemperature,c);
//			agent.setBirthPlace(birthPlace);			
//			//just age them into adults and add
//			agent.setAsRandomlyAgedAdult();
//			agent.setLifeStage(LifeStages.ADULT);
//			GeometryFactory fac = new GeometryFactory();
//			agent.setDominantLethalGene(true);
//			agent.emerged = true;
//			context.add(agent);
//			space.moveTo(agent, birthPlace.x,birthPlace.y);
//		}
	}
	/**
	 * Loads features from the specified shapefile.  The appropriate type of agents
	 * will be created depending on the geometry type in the shapefile (point, 
	 * line, polygon).
	 * 
	 * @param filename the name of the shapefile from which to load agents
	 * @param context the context
	 * @param geography the geography
	 */
	private void loadFeatures (String filename, Context<Object> context,  ContinuousSpace<Object> space){
		
		URL url = null;
		try {
			url = new File(filename).toURL();
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}

		List<SimpleFeature> features = new ArrayList<SimpleFeature>();
		
		// Try to load the shapefile
		SimpleFeatureIterator fiter = null;
		ShapefileDataStore store = null;
		store = new ShapefileDataStore(url);

		try {
			fiter = store.getFeatureSource().getFeatures().features();

			while(fiter.hasNext()){
				features.add(fiter.next());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			fiter.close();
			store.dispose();
		}
		boolean excluded = false;
		// For each feature in the file
		for (SimpleFeature feature : features){
			Geometry geom = (Geometry)feature.getDefaultGeometry();
			Object agent = null;

			// For Polygons, create ZoneAgents
			if (geom instanceof MultiPolygon){
				MultiPolygon mp = (MultiPolygon)feature.getDefaultGeometry();
				geom = (Polygon)mp.getGeometryN(0);

				// Read the feature attributes and assign to the ZoneAgent
				String name = (String)feature.getAttribute("id");
				int taxRate = (Integer)feature.getAttribute("dengue");

				//agent = new ZoneAgent(name);

				// Create a BufferZoneAgent around the zone, just for visualization
				/*Geometry buffer = GeometryUtils.generateBuffer(geography, geom, zoneDistance);
				BufferZoneAgent bufferZone = new BufferZoneAgent("Buffer: " + name, 
						zoneDistance, (ZoneAgent)agent);
				context.add(bufferZone);
				geography.move(bufferZone, buffer);*/
			}
			
			// For Points, create sources
			else if (geom instanceof Point){
				geom = (Point)feature.getDefaultGeometry();	
				
				// Read the feature attributes and assign to the ZoneAgent
				int type = (Integer)feature.getAttribute("DN");
				if(type == 1) {	
					agent = new CO2Source(convertFromGeographyToSpace(geom)[0],convertFromGeographyToSpace(geom)[1],co2Radius);
					
					//Geometry buffer = GeometryUtils.generateBuffer(geography, geom, co2Radius);
					//BufferZoneAgent bufferZone = new BufferZoneAgent("CO2Zone",	co2Radius, (ZoneAgent)agent);			
					//context.add(bufferZone);
					//geography.move(bufferZone, buffer);	
					//Add Breeding Spots 
					//if it's a house there may be a breeding spot
					double p = Math.random();
					if ( p < pDomesticBreedingSpot ){
						numberOfBreedingSpots++;
						
						BreedingSpot breedingSpot = new BreedingSpot(convertFromGeographyToSpace(geom)[0], convertFromGeographyToSpace(geom)[1], co2Radius);
						Geometry geom2 = (Point)feature.getDefaultGeometry();
						geom2.getCoordinate().x = geom.getCoordinate().x;
						geom2.getCoordinate().y = geom.getCoordinate().y;
						context.add(breedingSpot);
						space.moveTo(breedingSpot, convertFromGeographyToSpace(geom2));
						addMosquitoes(context, space, breedingSpot);
					}					
				} else if(type == 0) {
					agent = new Vegetation(convertFromGeographyToSpace(geom)[0],convertFromGeographyToSpace(geom)[1],co2Radius);
					//Geometry buffer = GeometryUtils.generateBuffer(geography, geom, co2Radius);
					//BufferZoneAgent bufferZone = new BufferZoneAgent("CO2Zone",	co2Radius, (ZoneAgent)agent);			
					//context.add(bufferZone);
					//geography.move(bufferZone, buffer);	
				} else {
					System.out.println("unkown area");
				}
				//org.opengis.referencing.crs.CoordinateReferenceSystem crs = geography.getCRS();
				//System.out.println(crs.getName());
				
			}

			/*// For Lines, create WaterLines
			else if (geom instanceof MultiLineString){
				MultiLineString line = (MultiLineString)feature.getDefaultGeometry();
				geom = (LineString)line.getGeometryN(0);

				// Read the feature attributes and assign to the ZoneAgent
				String name = (String)feature.getAttribute("Name");
				double flowRate = (Long)feature.getAttribute("Flow_Rate");
				agent = new WaterLine(name, flowRate);
			}*/

			if (agent != null){
				context.add(agent);
				space.moveTo(agent, convertFromGeographyToSpace(geom));
				//System.out.println(agent.getClass());
			}
			else if(!excluded){
				System.out.println("Error creating agent for  " + geom);
			}
			excluded = false;
		}				
	}
	private double[] convertFromGeographyToSpace(Geometry geom) {
		double result[] = {0,0};
		result[0] = 30 +((geom.getCoordinate().x + 81.78161 )/ (-81.78026 +81.78161)) *   150 ;
		result[1] = 15 +((geom.getCoordinate().y - 24.55281 )/ (24.55363 - 24.55281)) *   100 ;
		//result[0] = -1 * (geom.getCoordinate().x +81.78161 ) *  dimensions[0] / (-81.78026 +81.78161);
		//result[1] = -1 * (geom.getCoordinate().y - 24.55281 ) *  dimensions[1] / (24.55363 - 24.55281);
		return result;		
	}
}